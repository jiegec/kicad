
/* Do not modify this file, it was automatically generated by the
 * PNG2cpp CMake script, using a *.png file as input.
 */

#include <bitmaps_png/bitmaps_list.h>

static const unsigned char png[] = {
 0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a, 0x00, 0x00, 0x00, 0x0d, 0x49, 0x48, 0x44, 0x52,
 0x00, 0x00, 0x00, 0x18, 0x00, 0x00, 0x00, 0x18, 0x08, 0x04, 0x00, 0x00, 0x00, 0x4a, 0x7e, 0xf5,
 0x73, 0x00, 0x00, 0x00, 0x71, 0x49, 0x44, 0x41, 0x54, 0x38, 0xcb, 0x63, 0x60, 0xa0, 0x04, 0x48,
 0x6a, 0x4a, 0xed, 0x33, 0x66, 0x25, 0x41, 0x83, 0x94, 0xbf, 0xd4, 0x7f, 0x69, 0xe1, 0x51, 0x0d,
 0x83, 0x41, 0x83, 0x61, 0x9e, 0x49, 0x12, 0x04, 0x1a, 0xa8, 0x12, 0xa7, 0x61, 0xb2, 0x49, 0x17,
 0x08, 0x1a, 0x9f, 0x34, 0x49, 0x22, 0xc9, 0x49, 0x40, 0x4d, 0x03, 0xa7, 0xc1, 0x98, 0x55, 0x4e,
 0x10, 0x02, 0x25, 0xa3, 0xa5, 0xfe, 0xcb, 0x2a, 0x43, 0xd8, 0x86, 0x93, 0x8c, 0x72, 0x60, 0xe2,
 0x30, 0x08, 0x4d, 0xc9, 0x52, 0xfb, 0xa4, 0xfe, 0x13, 0x09, 0xf7, 0x81, 0x35, 0x48, 0x68, 0x49,
 0x87, 0x42, 0xa0, 0x54, 0x17, 0x50, 0x30, 0x09, 0xc6, 0xc3, 0x84, 0x12, 0x5a, 0xc3, 0x30, 0x2d,
 0x91, 0x5c, 0x08, 0x10, 0x03, 0x00, 0x05, 0x0b, 0x53, 0x01, 0xea, 0xcf, 0x7a, 0x74, 0x00, 0x00,
 0x00, 0x00, 0x49, 0x45, 0x4e, 0x44, 0xae, 0x42, 0x60, 0x82,
};

const BITMAP_OPAQUE lines90_xpm[1] = {{ png, sizeof( png ), "lines90_xpm" }};

//EOF
