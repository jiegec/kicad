
/* Do not modify this file, it was automatically generated by the
 * PNG2cpp CMake script, using a *.png file as input.
 */

#include <bitmaps_png/bitmaps_list.h>

static const unsigned char png[] = {
 0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a, 0x00, 0x00, 0x00, 0x0d, 0x49, 0x48, 0x44, 0x52,
 0x00, 0x00, 0x00, 0x18, 0x00, 0x00, 0x00, 0x18, 0x08, 0x04, 0x00, 0x00, 0x00, 0x4a, 0x7e, 0xf5,
 0x73, 0x00, 0x00, 0x00, 0xb5, 0x49, 0x44, 0x41, 0x54, 0x38, 0xcb, 0x63, 0x60, 0xa0, 0x23, 0x08,
 0x14, 0x0b, 0x39, 0x13, 0x72, 0x98, 0x04, 0xe5, 0xa1, 0x97, 0x43, 0xfe, 0x87, 0x9e, 0x22, 0x52,
 0x79, 0xa8, 0x68, 0xc8, 0xa5, 0x90, 0xff, 0x21, 0xd7, 0x43, 0x25, 0x06, 0x81, 0x72, 0xa0, 0x86,
 0x53, 0x40, 0xe5, 0x5f, 0x81, 0x18, 0x3b, 0x7c, 0x1b, 0xba, 0x29, 0x38, 0xb2, 0x81, 0x89, 0x78,
 0x0d, 0x10, 0x78, 0x26, 0x54, 0x11, 0xc3, 0x49, 0x41, 0x92, 0x58, 0x6c, 0x67, 0x8e, 0x90, 0x0d,
 0x49, 0x0f, 0xbe, 0x0f, 0x94, 0x7f, 0x49, 0xa4, 0x16, 0x10, 0xf0, 0xe3, 0x0d, 0xd9, 0x09, 0xb2,
 0x05, 0xc9, 0x61, 0x84, 0xb5, 0x80, 0x6d, 0x89, 0x20, 0x21, 0xe2, 0x42, 0xd2, 0x81, 0x1a, 0x36,
 0x90, 0x90, 0x34, 0x42, 0xe5, 0x80, 0x1a, 0x9e, 0x90, 0x90, 0xd2, 0x42, 0xd9, 0x80, 0x1a, 0x7e,
 0x90, 0xa2, 0x01, 0x64, 0xc3, 0x53, 0x12, 0x34, 0x04, 0x67, 0x60, 0xf8, 0x01, 0x1f, 0x80, 0x84,
 0x52, 0x68, 0x38, 0xd1, 0xca, 0x43, 0x77, 0xa0, 0xc5, 0x03, 0x4e, 0x97, 0x33, 0x87, 0xca, 0x01,
 0x1d, 0xf3, 0x00, 0x2d, 0xa6, 0xb1, 0x84, 0xfa, 0x61, 0x3c, 0x69, 0x09, 0xbf, 0x86, 0xe0, 0xb7,
 0x21, 0x1b, 0x42, 0x22, 0x88, 0x70, 0x0c, 0xd5, 0x00, 0x00, 0x9b, 0x0f, 0x98, 0x92, 0x49, 0x1a,
 0xa1, 0x56, 0x00, 0x00, 0x00, 0x00, 0x49, 0x45, 0x4e, 0x44, 0xae, 0x42, 0x60, 0x82,
};

const BITMAP_OPAQUE leave_sheet_xpm[1] = {{ png, sizeof( png ), "leave_sheet_xpm" }};

//EOF
